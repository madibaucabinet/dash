Notable changes
===============

* Support for the BIP70 Payment Protocol has been dropped from Dash Qt. Interacting with BIP70-formatted URIs will return an error message informing them of support removal. The `allowselfsignedrootcertificates` and `rootcertificates` launch arguments are no longer valid.
